/*
* Controller
*/
angular.module('urlshortenerapp', [])
    .controller('AppCtrl', function($scope, $http){
    console.log("Hello from shortener -controller-");

    var refresh = function(){
        $http.get('urllist').success(function(response){
            console.log("Data request gotten");
            $scope.urllist = response;
            $scope.contact = "";
        });
    };

    refresh();

    $scope.shortenURL = function(){
        console.log($scope.contact);
        $http.post('/urllist', $scope.url).success(function(res){
            console.log(res);
            refresh();
        }).error(function(res){
            console.log(res);
            alert(res);
        });
    };

    $scope.remove = function(id){
        console.log(id);
        $http.delete('/urllist/' + id).success(function(res){
            refresh();
        });
    };

    $scope.edit = function(id){
        console.log(id);
        $http.get('/urllist/' + id).success(function(res){
            $scope.contact = res;
        });
    };

    $scope.update = function(){
        console.log($scope.contact._id);
        $http.put('/urllist/' + $scope.contact._id, $scope.contact).success(function(res){
            refresh();
        });
    };

    $scope.deselect = function(){
        $scope.contact = " ";
    };

});
