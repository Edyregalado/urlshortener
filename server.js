var express = require('express'),
    app     = express(),
    mongojs = require('mongojs'),
    db = mongojs('urllist', ['urllist']),
    bodyParser = require('body-parser'),
    Hashids = require('hashids');

/*
*
*/
app.use(express.static(__dirname + '/client'));
app.use(bodyParser.json());

app.get('/urllist', function (req, res){
    console.log("GET request received")
    db.urllist.find(function (err, docs){
        console.log(docs);
        res.json(docs.map(function(doc){
            doc.short_url = "http://localhost:3000/" + doc.short_url;
            return doc;
        }));
    });
});

app.post('/urllist', function(req, res){
    console.log(req.body);
    //Section to generate shorten URL automatically
    var protocol = /^https?:\/\//ig;
    if(!protocol.test(req.body.base_url)){
        req.body.base_url = "http://" + req.body.base_url;
    }
    db.urllist.findOne({base_url: req.body.base_url}, function(err, doc){
        if(doc){
            console.log("IF!!");
            res.json(doc);
        }
        else{
            console.log("ELSE!!");
            if(req.body.short_url == undefined){
                var timestamp = Math.floor(Date.now()%Math.pow(62,3));
                var hashids = new Hashids("constant8549607477", 0),
                    id = hashids.encode(timestamp);

                //Shorten URL using vowel removal.
                //req.body.short_url = req.body.base_url.replace( /[aeiou.\-_=&\?%\$\/]/g, '' );
                req.body.short_url = id;

                console.log(req.body.short_url);
                //console.log("Hashids success: " + hashids);
            }
            else{
                db.urllist.findOne({short_url: req.body.short_url}, function(err, doc){
                    var errreturn = "Short_url already taken";
                    if(doc){
                        res.status(400).end(errreturn);
                        return;
                    }
                });
            }
            //Section to generate custom shorten URL
            db.urllist.insert(req.body, function(err, doc){
                doc.short_url = "http://localhost:3000/" + doc.short_url;
                res.json(doc);
            });
        }
    });


});

app.delete('/urllist/:id', function(req, res){
    var id = req.params.id;
    console.log(id);
    db.urllist.remove({_id: mongojs.ObjectId(id)}, function(err, doc){
        res.json(doc);
    });
});

app.get('/urllist/:id', function(req, res){
    var id = req.params.id;
    console.log(id);
    db.urllist.findOne({_id: mongojs.ObjectId(id)}, function(err, doc){
        doc.short_url = "http://localhost:3000/" + doc.short_url;
        res.json(doc);
    });
});

app.put('/urllist/:id', function(req, res){
    var id = req.params.id;
    console.log(req.body.base_url);
    db.urllist.findAndModify({
        query:{_id: mongojs.ObjectId(id)},
        update:{$set: {base_url: req.body.base_url, short_url: req.body.short_url}},
        new: true}, function(err, doc){
            res.json(doc);
        });
});

app.get('/:short_url', function(req, res){
    var id = req.params.short_url;
    console.log(id);
    db.urllist.findOne({short_url: id}, function(err, doc){
        if(doc && doc.base_url){
            res.redirect(doc.base_url);
        }
        else{
            res.redirect(404, '/');
        }
    });
});

app.listen(3000, function(){
    console.log('Awesome URL shortener is listening....');
});
